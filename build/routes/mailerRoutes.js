"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mailerController_1 = require("../controllers/mailerController");
class MailerRoutes {
    constructor() {
        this.router = (0, express_1.Router)();
        this.config();
    }
    config() {
        this.router.get('/', mailerController_1.mailerController.index);
        this.router.post('/', mailerController_1.mailerController.send);
        // this.router.get('/:id', indexController.traducir);
    }
}
const mailerRoutes = new MailerRoutes();
exports.default = mailerRoutes.router;
