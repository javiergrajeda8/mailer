"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const firebaseController_1 = require("../controllers/firebaseController");
class IndexRoutes {
    constructor() {
        this.router = (0, express_1.Router)();
        this.config();
    }
    config() {
        this.router.get('/', firebaseController_1.firebaseController.getConfiguration);
    }
}
const indexRoutes = new IndexRoutes();
exports.default = indexRoutes.router;
