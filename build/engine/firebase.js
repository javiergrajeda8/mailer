"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.fb = void 0;
const app_1 = __importDefault(require("firebase/app"));
require("firebase/firestore");
class Firebase {
    constructor() {
        const firebaseConfig = {
            apiKey: 'AIzaSyCCtejiJvqg3n0Xf76CDhJQ7lCoOLz1iY0',
            authDomain: 'ngresapp-jg.firebaseapp.com',
            projectId: 'ingresapp-jg',
            storageBucket: 'ingresapp-jg.appspot.com',
            messagingSenderId: '327251504047',
            appId: '1:327251504047:web:7946057a40aee3412f9d5a',
        };
        app_1.default.initializeApp(firebaseConfig);
    }
    getDB() {
        var db = app_1.default.firestore();
        return db;
    }
}
exports.fb = new Firebase();
