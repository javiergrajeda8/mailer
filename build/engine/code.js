"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.code = void 0;
const firebase_1 = require("../engine/firebase");
class Code {
    constructor() { }
    getCode() {
        return new Promise((resolve, reject) => {
            console.log('getCode');
            const db = firebase_1.fb.getDB();
            db.collection('configuration')
                .doc('random')
                .get()
                .then((code) => {
                console.log(code.exists);
                console.log(code.data());
                const d = code.data();
                const rnd = (Math.random() * parseInt(d.value, 0)).toFixed(0);
                db.collection('code')
                    .doc(rnd.toString())
                    .get()
                    .then((codeF) => {
                    console.log(codeF.exists);
                    if (codeF.exists) {
                        resolve(codeF.data());
                    }
                    else {
                        reject(null);
                    }
                });
            });
        });
    }
    getCodeInstance() {
        let code = new Code();
        return code;
    }
}
exports.code = new Code();
