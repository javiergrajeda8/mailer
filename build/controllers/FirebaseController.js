"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.firebaseController = void 0;
const http_status_codes_1 = require("http-status-codes");
class FirebaseController {
    getConfiguration(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            if (req.query.env) {
                if (req.query.env === 'p') {
                    res.status(http_status_codes_1.StatusCodes.OK).json({
                        timestamp: Date.now(),
                        configuration: {
                            apiKey: 'AIzaSyCCtejiJvqg3n0Xf76CDhJQ7lCoOLz1iY0',
                            authDomain: 'ingresapp-jg.firebaseapp.com',
                            databaseURL: 'https://ingresapp-jg.firebaseio.com',
                            projectId: 'ingresapp-jg',
                            storageBucket: 'ingresapp-jg.appspot.com',
                            messagingSenderId: '327251504047',
                            appId: '1:327251504047:web:7946057a40aee3412f9d5a',
                            measurementId: 'G-HJ5VCSTSWD',
                        },
                    });
                }
                else if (req.query.env === 'd') {
                    res.status(http_status_codes_1.StatusCodes.OK).json({
                        timestamp: Date.now(),
                        configuration: {
                            apiKey: 'AIzaSyCCtejiJvqg3n0Xf76CDhJQ7lCoOLz1iY0',
                            authDomain: 'ingresapp-jg.firebaseapp.com',
                            databaseURL: 'https://ingresapp-jg.firebaseio.com',
                            projectId: 'ingresapp-jg',
                            storageBucket: 'ingresapp-jg.appspot.com',
                            messagingSenderId: '327251504047',
                            appId: '1:327251504047:web:7946057a40aee3412f9d5a',
                            measurementId: 'G-HJ5VCSTSWD',
                        },
                    });
                }
                else {
                    res.status(http_status_codes_1.StatusCodes.BAD_REQUEST).json({ timestamp: Date.now(), message: 'Bad request' });
                }
            }
            else {
                res.status(http_status_codes_1.StatusCodes.BAD_REQUEST).json({ timestamp: Date.now(), message: 'Bad request' });
            }
        });
    }
}
exports.firebaseController = new FirebaseController();
