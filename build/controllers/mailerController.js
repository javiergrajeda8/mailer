"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.mailerController = void 0;
const nodemailer_1 = require("nodemailer");
class MailerController {
    index(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            // const { id } = req.params;
            // console.log('1');
            // const games = await pool.query('CALL sp_arbol(?)', [id]);
            res.json({ message: 'Mailer service is online 20220718!!!' });
        });
    }
    //   public async traducir(req: Request, res: Response) {
    //     const { id } = req.params;
    //     const games = await pool.query("CALL sp_decodificar('');"[id]);
    //     res.json(games);
    //   }
    send(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const transporter = (0, nodemailer_1.createTransport)({
                name: 'mail.javiergrajeda.site',
                host: 'mail.javiergrajeda.site',
                port: 465,
                secure: true,
                auth: {
                    user: 'info@javiergrajeda.site',
                    pass: 'infantesgoku8'
                }
            });
            const { to, subject, body } = req.body;
            const mailOptions = {
                from: 'info@javiergrajeda.site',
                to: to,
                subject: subject,
                text: body + '</br>' + Date.now()
            };
            transporter.sendMail(mailOptions, function (error, info) {
                //     // console.log('Something...');
                if (error) {
                    res.status(500).send({ error });
                    //   console.log(error);
                }
                else {
                    const response = 'Email sent: ' + info.response + ' to ' + to + ' subject ' + subject + ' body ' + body;
                    res.status(200).json({ "data": response });
                }
            });
            // res.json({ message: 'Email send' });
        });
    }
}
exports.mailerController = new MailerController();
