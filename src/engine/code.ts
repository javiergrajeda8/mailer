import { fb } from '../engine/firebase';
import { Configuration } from '../models/configuration';

class Code {
  constructor() {}

  public getCode() {
    return new Promise((resolve, reject) => {
      console.log('getCode');
      const db = fb.getDB();
      db.collection('configuration')
        .doc('random')
        .get()
        .then((code) => {
          console.log(code.exists);
          console.log(code.data());
          const d = code.data() as Configuration;
          const rnd = (Math.random() * parseInt(d.value, 0)).toFixed(0);
          db.collection('code')
            .doc(rnd.toString())
            .get()
            .then((codeF) => {
              console.log(codeF.exists);
              if (codeF.exists) {
                resolve(codeF.data());
              } else {
                reject(null);
              }
            });
        });
    });
  }

  public getCodeInstance() {
    let code = new Code();
    return code;
  }
}

export const code = new Code();
