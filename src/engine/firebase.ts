import firebase from 'firebase/app';
import 'firebase/firestore';

class Firebase {
  constructor() {
    const firebaseConfig = {
      apiKey: 'AIzaSyCCtejiJvqg3n0Xf76CDhJQ7lCoOLz1iY0',
      authDomain: 'ngresapp-jg.firebaseapp.com',
      projectId: 'ingresapp-jg',
      storageBucket: 'ingresapp-jg.appspot.com',
      messagingSenderId: '327251504047',
      appId: '1:327251504047:web:7946057a40aee3412f9d5a',
    };
    firebase.initializeApp(firebaseConfig);
  }

  getDB() {
    var db = firebase.firestore();
    return db;
  }
}
export const fb = new Firebase();
