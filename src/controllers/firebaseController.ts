import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';

class FirebaseController {
  public async getConfiguration(req: Request, res: Response) {
    if (req.query.env) {
      if (req.query.env === 'p') {
        res.status(StatusCodes.OK).json({
          timestamp: Date.now(),
          configuration: {
            apiKey: 'AIzaSyCCtejiJvqg3n0Xf76CDhJQ7lCoOLz1iY0',
            authDomain: 'ingresapp-jg.firebaseapp.com',
            databaseURL: 'https://ingresapp-jg.firebaseio.com',
            projectId: 'ingresapp-jg',
            storageBucket: 'ingresapp-jg.appspot.com',
            messagingSenderId: '327251504047',
            appId: '1:327251504047:web:7946057a40aee3412f9d5a',
            measurementId: 'G-HJ5VCSTSWD',
          },
        });
      } else if (req.query.env === 'd') {
        res.status(StatusCodes.OK).json({
          timestamp: Date.now(),
          configuration: {
            apiKey: 'AIzaSyCCtejiJvqg3n0Xf76CDhJQ7lCoOLz1iY0',
            authDomain: 'ingresapp-jg.firebaseapp.com',
            databaseURL: 'https://ingresapp-jg.firebaseio.com',
            projectId: 'ingresapp-jg',
            storageBucket: 'ingresapp-jg.appspot.com',
            messagingSenderId: '327251504047',
            appId: '1:327251504047:web:7946057a40aee3412f9d5a',
            measurementId: 'G-HJ5VCSTSWD',
          },
        });
      } else {
        res.status(StatusCodes.BAD_REQUEST).json({ timestamp: Date.now(), message: 'Bad request' });
      }
    } else {
      res.status(StatusCodes.BAD_REQUEST).json({ timestamp: Date.now(), message: 'Bad request' });
    }
  }
}

export const firebaseController = new FirebaseController();
