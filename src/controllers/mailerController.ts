import { Request, Response } from 'express';
import { createTransport } from 'nodemailer';

class MailerController {
  public async index(req: Request, res: Response) {
    // const { id } = req.params;
    // console.log('1');
    // const games = await pool.query('CALL sp_arbol(?)', [id]);
    res.json({ message: 'Mailer service is online 20220718!!!' });
  }

  //   public async traducir(req: Request, res: Response) {
  //     const { id } = req.params;
  //     const games = await pool.query("CALL sp_decodificar('');"[id]);

  //     res.json(games);
  //   }
  public async send(req: Request, res: Response) {
    const transporter = createTransport({
      name: 'mail.javiergrajeda.site',
      host: 'mail.javiergrajeda.site',
      port: 465,
      secure: true, // use SSL
      auth: {
        user: 'info@javiergrajeda.site',
        pass: 'infantesgoku8'
      }
    });
    const { to, subject, body } = req.body;
    const mailOptions = {
      from: 'info@javiergrajeda.site',
      to: to,
      subject: subject,
      text: body + '</br>' + Date.now()
    };

    transporter.sendMail(mailOptions, function(error, info) {
      //     // console.log('Something...');
      if (error) {
        res.status(500).send({ error });
        //   console.log(error);
      } else {
        const response = 'Email sent: ' + info.response + ' to ' + to + ' subject ' + subject + ' body ' + body;
        res.status(200).json({"data": response});
      }
    });
    // res.json({ message: 'Email send' });
  }
}

export const mailerController = new MailerController();
