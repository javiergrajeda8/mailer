import { Request, Response } from 'express';
import { fb } from '../engine/firebase';
import { StatusCodes } from 'http-status-codes';
import { code } from '../engine/code';

class CodeController {
  public async gc(req: Request, res: Response) {
    const rnd = (Math.random() * 1000).toFixed(0);
    const c = code
      .getCode()
      .then((code: any) => {
        const params = { code: code.value + rnd.toString() };        
        console.log('CODE', params);
        res.status(StatusCodes.OK).json({ timestamp: Date.now(), code: params.code });
    })
      .catch((error) => {
        console.log('ERROR', error);
      });
  }

}

export const codeController = new CodeController();
