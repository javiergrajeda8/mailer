import { Request, Response } from 'express';
import { fb } from '../engine/firebase';
import { StatusCodes } from 'http-status-codes';

class GoogleController {
  public async getApiKey(req: Request, res: Response) {
    const db = fb.getDB();
    db.collection('configuration')
      .doc('apiKey')
      .get()
      .then((apiKey) => {
        if (apiKey.exists) {
          const API_KEY = apiKey.data() as any;
          res.status(StatusCodes.OK).json({ timestamp: Date.now(), apiKey: API_KEY.value });
        } else {
          res.status(StatusCodes.NOT_FOUND).json({ timestamp: Date.now() });
        }
      });
  }
}

export const googleController = new GoogleController();
