import { Router } from 'express';
import { mailerController } from '../controllers/mailerController';

class MailerRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  config(): void {
    this.router.get('/', mailerController.index);
    this.router.post('/', mailerController.send)
    // this.router.get('/:id', indexController.traducir);
  }
}

const mailerRoutes = new MailerRoutes();
export default mailerRoutes.router;
