import { Router } from 'express';
import { firebaseController } from '../controllers/firebaseController';

class IndexRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  config(): void {
    this.router.get('/', firebaseController.getConfiguration);
  }
}

const indexRoutes = new IndexRoutes();
export default indexRoutes.router;
